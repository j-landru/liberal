L.I.B.E.R.A.L.
==============
***L***inux ***I***nside ***B***rowser to ***E***xperiment ***R***emotely ***A***ctivities and ***L***abs

**----- oOo -----**

## Abstract ##
L.I.B.E.R.A.L. (*L*inux *I*nside *B*rowser to *E*xperiment *R*emotely *A*ctivities and *L*abs) is a JSLIinux Alpine x86 image to play CLI (Command Line Interface) basic Linux labs. JSLinux is a javascript riscV-64 and x86 emulator, from Fabrice Bellard [1], to run a Linux VM inside the browser.

## Context ##

Several Javascript CPU emulators (jor1k [5], TinyEMU [2], JSLinux [1], v86, angel and more[6]...) are nowadays available to run some small basic virtual machines inside a browser. Performance of such emulated machines are modest *(on common host with a recent browser version, performance are near about one hundred mips, or a bit more)*, but sufficient for experimentations or some interactive CLI *(Command Line Interface)* activities. Some MOOCs *(Massive Open Online Course)* on France Universite Numérique portal as FUN langace C, or FUN Bash use such VM to distribute lab plateforms at scale, with fixed provider costs *(no need to deploy and scale up large VM fleets in the cloud, as lab VM is run on personnal student's host)* as well as no hypervisor dependency *(no need to install and configure local hypervisor as KVM, Virtualbox, VMplayer,...)*, nor HVM *(Hardware Virtual Machine)* extensions activation on student's host.

LIBERAL is another in-browser remote lab use case based on such javascript CPU emulator, designed at IMT Lille Douai in that unusual 2020 COVID19 pandemic period where confinment "impose" to play some labs remotely at student's home. It is based above Fabrice Bellard's Tinyemu/JSLinux emulator *(a RiscV and x86 javascript emulator)* [1]. Unlike other CPU emulators, where the VM linux root filesystem has to be built from scratch using low level buildroot tool, JSLinux x86 emulator can use some 32 bits classical linux distribution with large package catalog. Fabrice Bellard provides a very light x86 Alpine root filesystem on its demo site [1]. Alpine Linux [3], is one of the latest Linux distribution still available for x86 arch. LIBERAL lab is just a personalized JSLinux Alpine x86 image with dedicated packages needed by the lab.

***Nota :*** *LIBERAL JSLinux VM are ephemeral stateless virtual machines, restarting from initial state each time you reload the VM web page. At this stage, don't hope deploying multi session labs. One way to investigate for stateful VM could be overlaying COW (copy on write with overlayfs) storage through websocket network connection, but that's another story ?...*

***Nota :*** *At this stage LIBERAL VM are networkconnectionless. JSLinux VM, as jor1k VM can have network attachment through a websocket relay deployed on dedicated portal. This facility is not yet available on LIBERAL labs.*

### Advantages ###

* Linux sandboxed VM inside common browser : *(Mozilla Firefox or Google's Chrome recommended but seems to work also on KDE konqueror or Microsoft Edge)*,
* deployable at large scale and fixed cost as only 1 webpage and 1 image download per user on provider side, 
* clientless (no local hypervisor need, no HVM (Intel VT-x or AMD-V) extensions needs on student's host,
* ephemeral stateless sandboxed VM : simply restart at primitve state by relaoding VM web page, 

### Limitations ###

* modest performance, but sufficient for some interactive CLI activities,
* 32 bits x86 architecture only,
* no persistence only ephemeral stateless VM,
* no network connection or limited network connection through a websocket relay,
* CLI only or limited graphical interface
* ephemeral stateless VM : unable to play multisession labs,
* at this stage i'm unable to build another kernel other the default one provided by JSLIinux, so no IPv6 ;o{

## LIBERAL lab collection ##

### L.O.B.S.T.E.R. ### 

**L.O.B.S.T.E.R.** (***L***dap ***O***pen ***B***ase ***S***ervice to ***T***rain for ***E***ntries and ***R***eferrals) is my first lab of "LIBERAL lab collection". It's a basic lab, providing activities about LDAP (*L*ightweight *D*irectory *A*pplication *P*rotocol) directory entries and referrals. Used to illustrate a LDAP lecture, it embeds a local Openldap server and three associated pedagogical DIT (Directory Information Tree). Yes, modest performance of JSLinux VM is sufficient for such pedagogical server plateform.

*Lobster LDAP directory, provides a pedagogical dataset composed of three small DIT (Directory Information Tree) :*
* *a white pages directory (about 10 thousand fictious people entries),*
* *a posixaccount profile directory (about 5 hundred fictious posix account profiles),*
* *a specific dedicated IMT Lille Douai directory (about 1 thousand entries) providing IMTLD academic module referential catalog, illustrating how to create a specific LDAP schema and it associated DIB *(Directory Inbformation Base)*.

***Want to taste lobster with liberal sauce ?***

*Just launch https://j-landru.gitlab.io/liberal/?cpu=x86&mem=256&cols=120&rows=30 in your browser (Mozilla Firefox or Google's Chrome recommended but seems to work also on KDE konqueror or Microsoft Edge)* ```login/passwd``` *is* ```ldaptp/ldaptp```

*The recipe for the liberal sauce for LOBSTER is available under CC-BY-SA license, at https://gitlab.com/j-landru/liberal/-/snippets/2028201  If you want to adjust it to your liking adding new Alpine ingredients, just do it and enjoy !*


**-----oOo-----**

## URLography : ## 

*  [1] JSLinux : https://bellard.org/JSLinux/ , https://bellard.org/JSLinux/tech.html , https://bellard.org/JSLinux/tech.html
*  [2] TinyEMU : https://bellard.org/tinyemu/ , https://bellard.org/tinyemu/readme.txt
*  [3] Alpine Linux donwloads : https://wiki.alpinelinux.org/wiki/Installation
*  [4] Alpine installation : https://wiki.alpinelinux.org/wiki/Installation
*  [5] jor1k : https://github.com/s-macke/jor1k , https://github.com/s-macke/jor1k/wiki
*  [6] Similar emulators written in Javascript : https://github.com/s-macke/jor1k/wiki/Similar-emulators-written-in-Javascript
